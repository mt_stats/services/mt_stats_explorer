import Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :mt_stats_explorer, MtStatsExplorerWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "50dyIO7RnjR/XgCQumvM2dFG+QRI4hl8L77uv+dCk2szxRzp3KDMxszZJwSJA4Ta",
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime

config :mt_stats, MtStats.Repo,
  username: "postgres",
  password: "postgres",
  hostname: "localhost",
  database: "mt_stats_test#{System.get_env("MIX_TEST_PARTITION")}",
  pool: Ecto.Adapters.SQL.Sandbox,
  pool_size: 10
