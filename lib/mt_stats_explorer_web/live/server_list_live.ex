defmodule MtStatsExplorerWeb.ServerListLive do
  use MtStatsExplorerWeb, :live_view
  use Phoenix.Component
  import Ecto.Query
  alias MtStats.{Repo, Server}

  defp table_header(assigns) do
    ~H"""
    <thead>
      <tr>
        <th>Name</th>
        <th>Address</th>
        <th>Online</th>
        <th>Stats</th>
      </tr>
    </thead>
    """
  end

  defp table_row(assigns) do
    ~H"""
    <tr id={"server-#{@server.id}"}>
      <td>
        <%= if !is_nil(@server.url) do %>
          <a href={@server.url} target="_blank"><%= @server.name %></a>
        <% else %>
          <%= @server.name %>
        <% end %>
      </td>
      <td><%= @server.address %></td>
      <td class="clients_current"><%= @server.clients_current %></td>
      <td>
        <span>
          <a data-phx-link="redirect"
             data-phx-link-state="push"
             href={"/server/#{@server.id}/#{@server.address}"}>Show</a>
        </span>
      </td>
    </tr>
    """
  end

  # style credit: https://stackoverflow.com/questions/52387423/make-table-responsive-on-mobile-devices-using-html-css
  def render(assigns) do
    ~H"""
    <style>

    .clients_current {
    text-align:center;
    vertical-align:middle;
    }

    @media
    only screen 
    and (max-width: 760px), (min-device-width: 768px) 
    and (max-device-width: 1024px)  {

    /* Force table to not be like tables anymore */
    table, thead, tbody, th, td, tr {
    display: block;
    }

    /* Hide table headers (but not display: none;, for accessibility) */
    thead tr {
    position: absolute;
    top: -9999px;
    left: -9999px;
    }

    td {
    /* Behave  like a "row" */
    position: relative;
    padding-left: 50%;
    }

    .clients_current {
    text-align:inherit;
    vertical-align:inherit;
    }


    td:nth-of-type(3):before { content: "Online: "; }
    }
    </style>

    <table>
      <tbody id="solid_blocks">
        <.table_header />
        <%= for server <- @servers do %>
          <.table_row server={server} />
        <% end %>
      </tbody>
    </table>
    """
  end

  def mount(_params, _session, socket) do
    servers_query =
      from(s in Server,
        order_by: {:desc, s.clients_current},
        where: not is_nil(s.clients_current)
      )

    servers =
      servers_query
      |> Repo.all()
      |> Enum.map(&%{&1 | address: "#{&1.address}:#{&1.port}"})

    {:ok,
     assign(socket,
       servers: servers,
       page_title: "Server List",
       page_type: "home"
     )}
  end
end
