defmodule MtStatsExplorerWeb.ServerStatsLive do
  use MtStatsExplorerWeb, :live_view
  use Phoenix.Component
  alias MtStats.{Repo, Server, DateRange}
  alias MtStatsExplorerWeb.{ServerListLive, ServerStatsLive}
  require Logger
  
  alias MtStatsExplorerWeb.Router.Helpers, as: Routes
  alias MtStatsExplorerWeb.Endpoint

  @ranges [
    "1 hour",
    "6 hours",
    "1 day",
    "2 days",
    "3 days",
    "1 week",
    "2 weeks",
    "3 weeks",
    "1 month",
    "1 year"
  ]
  @default_range "1week"

  def render(assigns) do
    ~H"""
    <div phx-window-keydown="keydown"> </div>
    <div class="left-info">
      <h2><%= @server.name %></h2>
      <span><b>Address:</b> <%= "#{@server.address}:#{@server.port}" %></span>
      <span><b>Description:</b> <%= @server.description %></span>
      <span><b>Game:</b> <%= @server.gameid %></span>
      <span><b>Privs:</b> <%= @server.privs %></span>
      <span><b>PVP:</b> <%= @server.pvp %></span>
    </div>
    <div class="right-info">
      <div style="margin-left: 20px">
        <.form let={f} for={@date_range_changeset} phx-change="date_range_change">
        <div style="display: flex; gap: 12px">
          <%= hidden_input f, :server_id, value: @server.id %>
          <div style="display: table-cell; vertical-align: middle;">
            <%= label f, :range %> 
            <%= select f, :range, @ranges %>
            <%= error_tag f, :range %>
          </div>
          <div style="display: table-cell; vertical-align: middle;">
            <%= label f, :date %> 
            <%= datetime_local_input f, :date %>
            <%= error_tag f, :date %>
          </div>
        </div>
        </.form>
      </div>
    </div>

    <div style="display: flex; flex-direction: column">
      <.plot stats={@stats} cols={:clients_avg} title="Number of Players"/>
      <.plot stats={@stats} cols={:ping_avg} title="Ping"/>
    </div>
    """
  end

  def mount(%{"id" => id}, _session, socket) do
    server = Repo.get(Server, id)

    if is_nil(server) do
      {:ok,
       push_redirect(socket,
         to: Routes.live_path(socket, ServerListLive)
       )}
    else
      {:ok,
       assign(socket,
         server: server,
         page_title: server.name,
         ranges: @ranges,
         page_type: "server",
       )
      }
    end
  end

  def handle_params(params = %{"range" => range}, _uri, socket) do
    id = socket.assigns.server.id
    params = %{server_id: id, range: range, date: params["date"]}

    case DateRange.try_params(params) do
      {:ok, date_range} ->
        {:noreply, assign(socket,
          date_range: date_range,
          date_range_changeset: DateRange.changeset(date_range, %{})
        )
        |> load_stats()
        }
      {:error, changeset} ->
        Logger.error("Wrong url query params: #{inspect(changeset.errors)}")
        {:noreply, patch_params(socket, range: @default_range)}
    end
  end

  def handle_params(_params, _uri, socket) do
    {:noreply, patch_params(socket, range: @default_range)}
  end

  defp patch_params(socket, params) do
    id = socket.assigns.server.id

    push_patch(socket,
      to: Routes.live_path(socket, ServerStatsLive, id, params))
  end

  defp init_date_range_changeset(server_id) do
    %DateRange{}
    |> DateRange.changeset(%{range: "1 week", server_id: server_id})
  end

  defp init_date_range(server_id) do
    init_date_range_changeset(server_id)
    |> Ecto.Changeset.apply_action!(:insert)
  end

  def plot(assigns) do
    plot = plot(assigns.stats, assigns.cols, assigns.title)

    ~H"""
    <%= plot %>
    """
  end

  def plot(%Contex.Dataset{data: []}, _fields, _title) do
    "No data"
  end

  def plot(dataset, fields, title) do
    params = [
      # mapping: %{x_col: :inserted_at, y_cols: List.wrap(fields)},
      mapping: %{x_col: :time, y_cols: List.wrap(fields)},
      smoothed: false
    ]

    {time, data} =
      :timer.tc(fn ->
        dataset
        |> Contex.Plot.new(Contex.LinePlot, 600, 400, params)
        |> Contex.Plot.titles(title, nil)
        |> Contex.Plot.to_svg()
      end)

    Logger.info("stats_plot_time #{time / 1000} ms, #{title}")
    data
  end

  def handle_event("keydown", %{"key" => "ArrowRight"}, socket) do
    IO.puts("RIGHT")

    {:noreply, socket}
  end

  def handle_event("keydown", %{"key" => "ArrowLeft"}, socket) do
    IO.puts("LEFT")

    {:noreply, socket}
  end

  def handle_event("keydown", key, socket) do
    {:noreply, socket}
  end

  def handle_event("date_range_change", %{"date_range" => params}, socket) do
    #new_params = %{
    #  range: String.replace(params["date_range"]["range"], " ", "")
    #}
    #new_params = if params["date_range"]["date"] != "" do
    #  Map.put(new_params, :date, params["date_range"]["date"])
    #else
    #  new_params
    #end

    date_range = socket.assigns.date_range

    maybe_new_date_range = date_range
                           |> DateRange.changeset(params)
                           |> Ecto.Changeset.apply_action(:insert)

    case maybe_new_date_range do
      {:ok, date_range} -> {:noreply, patch_params(socket, params)}
      {:error, changeset} ->
        {:noreply, assign(socket, date_range_changeset: changeset)}
    end
  end

  defp add_time_to_date(date) do
    if date == "" do
      ""
    else
      "#{date} 00:00:00"
    end
  end

  defp load_stats(socket) do
    date_range = socket.assigns.date_range
    load_stats(socket, date_range)
  end

  defp load_stats(socket, date_range) do
    stats =
      if !connected?(socket) do
        []
      else
        stats_query = DateRange.build_query(date_range)

        {time, stats} =
          :timer.tc(fn ->
            stats_query
            |> Repo.all()
            |> Enum.map(&Map.from_struct/1)
            |> Enum.map(
              &%{
                &1
                | clients_avg: Decimal.to_float(&1.clients_avg),
                  ping_avg: Decimal.to_float(&1.ping_avg)
              }
            )
          end)

        Logger.info("stats_query_time #{time / 1000} ms")
        stats
      end

    stats = Contex.Dataset.new(stats)

    assign(socket, stats: stats)
  end
end
