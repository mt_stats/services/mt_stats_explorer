defmodule MtStatsExplorerWeb.PageController do
  use MtStatsExplorerWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
