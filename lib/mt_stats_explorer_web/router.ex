defmodule MtStatsExplorerWeb.Router do
  use MtStatsExplorerWeb, :router
  import Phoenix.LiveDashboard.Router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {MtStatsExplorerWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :admins_only do
    plug :admin_basic_auth
  end

  # admin only
  scope "/" do
    pipe_through [:browser, :admins_only]
    live_dashboard "/dashboard"
  end

  defp admin_basic_auth(conn, _opts) do
    username = System.fetch_env!("ADMIN_USERNAME")
    password = System.fetch_env!("ADMIN_PASSWORD")
    Plug.BasicAuth.basic_auth(conn, username: username, password: password)
  end

  # public
  scope "/", MtStatsExplorerWeb do
    pipe_through :browser

    live_session :default do
      live "/", ServerListLive
      live "/server/:id", ServerStatsLive
      live "/server/:id/:address", ServerStatsLive
    end

    # get "/", PageController, :index
  end

  # Other scopes may use custom stacks.
  # scope "/api", MtStatsExplorerWeb do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  # if Mix.env() in [:dev, :test] do
  #  scope "/" do
  #    pipe_through :browser
  #
  #    live_dashboard "/dashboard", metrics: MtStatsExplorerWeb.Telemetry
  #  end
  # end
end
